# SPDX-License-Identifier: GPL-2.0-only
config ARCH_HAS_RESET_CONTROLLER
	bool

menuconfig RESET_CONTROLLER
	bool "Reset Controller Support"
	default y if ARCH_HAS_RESET_CONTROLLER
	help
	  Generic Reset Controller support.

	  This framework is designed to abstract reset handling of devices
	  via GPIOs or SoC-internal reset controller modules.

	  If unsure, say no.

if RESET_CONTROLLER

config RESET_SIMPLE
	bool "Simple Reset Controller Driver" if COMPILE_TEST
	help
	  This enables a simple reset controller driver for reset lines that
	  that can be asserted and deasserted by toggling bits in a contiguous,
	  exclusive register space.

	  Currently this driver supports:
	   - Altera 64-Bit SoCFPGAs
	   - ASPEED BMC SoCs
	   - Bitmain BM1880 SoC
	   - Realtek SoCs
	   - RCC reset controller in STM32 MCUs
	   - Allwinner SoCs
	   - SiFive FU740 SoCs


config RESET_IMX7
	bool "i.MX7 Reset Driver"
	default ARCH_IMX7
	select MFD_SYSCON
	help
	  This enables the reset controller driver for i.MX7 SoCs.

config RESET_STM32
	bool "STM32 Reset Driver"
	depends on ARCH_STM32MP || COMPILE_TEST
	help
	  This enables the reset controller driver for STM32MP1.

config RESET_STARFIVE
	bool "StarFive Controller Driver" if COMPILE_TEST
	default SOC_STARFIVE
	help
	  This enables the reset controller driver for the StarFive JH7100.

endif
